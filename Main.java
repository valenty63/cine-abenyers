import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main (String[] args) {
        int tiquetes, numAsiento, regreso = 0;
        ArrayList<Boolean> fila = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        for (int i=0;i<5; i++) {
            fila.add(false);
        }

        while (true) {
            int asientosLibres = 0, x = 1;
            for (Boolean i : fila) {
                if (i==false) {
                    asientosLibres++;
                }
            }

            System.out.println("Cuantos boletos desea comprar?");

            try {
                tiquetes = Integer.parseInt(br.readLine());
            } catch (Exception ex) {
                return;
            }

            if (tiquetes > asientosLibres) {
                System.out.println("El maximo  de asientos que puede comprar es: " + asientosLibres);
                continue;
            }
            System.out.println("Asientos Disponibles: ");

            for (boolean e : fila) {
                if (e == false) {
                    System.out.println("Asiento " + x);
                }
                x++;
            }

            System.out.println();

            for (int i=0;i<tiquetes;i++) {
                System.out.println("Boletos " + Integer.sum(i,1));
                System.out.println("Escoja su numero de asiento:");

                try {
                    numAsiento = Integer.parseInt(br.readLine());
                } catch (Exception ex) {
                    return;
                }

                if (fila.get(numAsiento - 1) == true) {
                    System.out.println("Este asiento no esta dispoible , escoja otro por favor .");
                    regreso = 0;
                } else {
                    fila.set(numAsiento - 1, true);
                    regreso = 1;
                }
            }


                System.out.println("Quiere realizar nuevamente la compra?");
                System.out.println("1. Si");
                System.out.println("2. No");
                try {
                    regreso = Integer.parseInt(br.readLine());
                } catch (Exception ex) {
                    return;
                }

                if (regreso == 1) {
                    continue;
                } else if (regreso == 2) {
                    return;
                } else {
                    return;
                }

            }
        }
    }